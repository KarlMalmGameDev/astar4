#pragma once
#include "stdafx.h"
#include "TextureManager.h"
class Tile
{
public:
	Tile(TextureManager* Tex, int x, int y);
	Tile();
	~Tile();
	bool isVisited, isDiscarded;
	void Draw(sf::RenderWindow& window);
	void setFValue(int Fnew);
	void setGValue(int Gnew);
	void setHValue(int Hnew);

	void setGoal();
	void setObstacle();
	void setClosed();
	void setOpened();
	void setStart();
	void setPath();
	void setFailed();

	void setParent(Tile* parentTile);

	void Reset();

	int GetFValue();
	int GetHValue();
	int GetGValue();
	int GetXPos();
	int GetYPos();
	bool GetGoal();
	bool GetObstacle();
	bool GetStart();
	bool GetClosed();
	bool GetOpened();
	Tile* GetParent();

private:
	sf::Sprite* TileSprite;
	sf::Texture* TileTexture;
	sf::Texture* ClosedTexture;
	sf::Texture* GoalTexture;
	sf::Texture* StartTexture;
	sf::Texture* FailTexture;
	sf::Texture* ObstacleTexture;
	sf::Texture* OpenTexture;
	sf::Texture* PathTexture;


	int fvalue, gvalue, hvalue = 0;
	bool open, closed, start, goal, obstacle = false; 
	sf::Vector2f pos;
	Tile* TileParent;
	TextureManager* TexMan;
};

