#pragma once
#include "stdafx.h"
class TextureManager
{
public:
	TextureManager();
	~TextureManager();
	void PreloadTextures();

	void CreateTexture(std::string filePath);

	sf::Texture* LoadTexture(std::string filePath);
private:
	std::map<std::string, sf::Texture*> textures;

};

