#pragma once
#include "stdafx.h"
class IEntity
{
public:
	IEntity();
	virtual ~IEntity();
	void setPosition(int p_X, int p_Y);
	sf::Sprite* GetSprite();
	std::pair <int, int> GetGridPosition();
private:
	sf::Sprite p_image;
	std::pair<int, int> p_gridPosition;
	
};

