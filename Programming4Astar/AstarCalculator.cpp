#include "AstarCalculator.h"



AstarCalculator::AstarCalculator(Tile* Tilemap[setup::GRIDWIDTH][setup::GRIDHEIGHT])
{
	for (int i = 0; i < setup::GRIDWIDTH; i++)
	{
		for (int j = 0; j < setup::GRIDHEIGHT; j++)
		{
			gridreference[i][j] = Tilemap[i][j];
		}
	}
}

std::vector<Tile*> AstarCalculator::CalculatePath(int startx, int starty, int endx, int endy)
{
	std::pair<int, int> currentPair;
	FindNodes(startx, starty);
	MoveToClosed(startx, starty);
	CalculateFGH(startx, starty, endx, endy);
	currentPair = FindLowestF();
	AStarRecurse(currentPair.first, currentPair.second, endx, endy);
	//FindNodes(startx, starty);
	//MoveToClosed(startx, starty);
	//FindLowestF();
	return std::vector<Tile*>();
}


AstarCalculator::~AstarCalculator()
{
}

void AstarCalculator::FindNodes(int indexX, int indexY)
{

	for (int i = -1; i <= 1 ; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			MoveToOpen((indexX+i), (indexY+j), gridreference[indexX][indexY]);
		}
	}
}

std::pair<int, int> AstarCalculator::FindLowestF()
{
	//TODO Add breakout condition for empty list
	std::pair<int, int> temppair;
	fCheck=500;
	for (auto it : openList)
	{
		if (openList.empty())
		{
			break;
		}
		if (it->GetFValue() < fCheck)
		{
			fCheck = it->GetFValue();
			temppair = std::make_pair(it->GetXPos()/setup::TILESIZE, it->GetYPos() / setup::TILESIZE);
		}
	}
	return temppair;
}

void AstarCalculator::MoveToOpen(int indexX, int indexY, Tile* parentTile)
{
	if (indexX < 0 || indexX >= setup::GRIDWIDTH || indexY < 0 || indexY >= setup::GRIDHEIGHT)
	{
		return;
	}
	if (std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]) != openList.end())
	{
		return;
	}
	if (std::find(closedList.begin(), closedList.end(), gridreference[indexX][indexY]) != closedList.end())
	{
		return;
	}
	/*if (gridreference[indexX][indexY]->GetObstacle())
	{
		return;
	}*/
	gridreference[indexX][indexY]->setParent(parentTile);
	openList.push_back(gridreference[indexX][indexY]);

}

void AstarCalculator::MoveToClosed(int indexX, int indexY)
{
	if (indexX < 0 || indexX >= setup::GRIDWIDTH || indexY < 0 || indexY >= setup::GRIDHEIGHT)
	{
		return;
	}
	if (std::find(closedList.begin(), closedList.end(), gridreference[indexX][indexY]) != closedList.end())
	{
		return;
	}
	if (std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]) != openList.end())
	{
		openList.erase(std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]));
	}
	closedList.push_back(gridreference[indexX][indexY]);

}

Tile* AstarCalculator::AStarRecurse(int startx, int starty, int endx, int endy)
{
	std::pair<int,int> currentPair;
	FindNodes(startx, starty);
	MoveToClosed(startx, starty);
	currentPair=FindLowestF();
	AStarRecurse(currentPair.first, currentPair.second, endx, endy);
	if (currentPair.first == endx && currentPair.second == endy)
	{
		return gridreference[currentPair.first][currentPair.second];
	}
	//TODO: Check if currentpair is end tile, if so, terminate loop
	return nullptr; //Fix this next time
}

void AstarCalculator::CalculateFGH(int currentX, int currentY, int endX, int endY)
{
	if (currentX != endX && currentY != endY)
	{
		gridreference[currentX][currentY]->setGValue(gridreference[currentX][currentY]->GetParent()->GetGValue() + 14);
	}
	else if (currentX == endX || currentY == endY)
	{
		gridreference[currentX][currentY]->setGValue(gridreference[currentX][currentY]->GetParent()->GetGValue() + 10);
	}

	gridreference[currentX][currentY]->setHValue((abs(endX - currentX) + abs(endY - currentY))*10);

	gridreference[currentX][currentY]->setFValue(
	gridreference[currentX][currentY]->GetHValue() + gridreference[currentX][currentY]->GetGValue()
	);

}

