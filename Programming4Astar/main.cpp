// Programming4Astar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TextureManager.h"
#include "SpriteManager.h"
#include "Tile.h"
#include "AstarCalculator.h"
#include "Config.h"
#include <vector>
#include <iostream>

int running = 1;
const int straightmove = 10;
const int diagonalmove = 14;
TextureManager* texMan;
Tile* tilemap[setup::GRIDWIDTH][setup::GRIDHEIGHT];
Tile* currentTile;
Tile* lowestF = NULL;
sf::Vector2i startPos;
sf::Vector2i goalPos;
sf::Sprite* characterSprite;
sf::Texture* characterTexture;


void RandGoal()
{
	srand(time(NULL));
	int randWidth = rand() % setup::GRIDWIDTH;
	int randHeight = rand() % setup::GRIDHEIGHT;
	goalPos = sf::Vector2i(randWidth*setup::TILESIZE, randHeight*setup::TILESIZE);
	tilemap[randWidth][randHeight]->setGoal();
}

void RandStart()
{
	int randWidth, randHeight;
	do {

		srand(time(NULL));
		randWidth = rand() % setup::GRIDWIDTH;
		randHeight = rand() % setup::GRIDHEIGHT;
		startPos = sf::Vector2i(randWidth*setup::TILESIZE, randHeight*setup::TILESIZE);
	} while (startPos == goalPos);
		tilemap[randWidth][randHeight]->setStart();
}
int main()
{
	sf::RenderWindow window(sf::VideoMode(1024, 768), "A star");	
	texMan = new TextureManager;
	texMan->PreloadTextures();
	for (int i = 0; i < setup::GRIDWIDTH; i++)
	{
		for (int j = 0; j < setup::GRIDHEIGHT; j++)
		{
			tilemap[i][j] = new Tile(texMan, i*setup::TILESIZE, j*setup::TILESIZE);
		}
	}
	RandGoal();
	RandStart();
	characterTexture = texMan->LoadTexture("../External/sprites/Character.png");
	characterSprite = new sf::Sprite;
	characterSprite->setTexture(*characterTexture);
	characterSprite->setPosition(sf::Vector2f(startPos));
	AstarCalculator calculator(tilemap);

	while (window.isOpen()) //Main Game Loop
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type) {

				case sf::Event::Closed:
					window.close();
					break;
				
				case sf::Event::KeyPressed:
					if (event.key.code == sf::Keyboard::Space)
					{
						calculator.CalculatePath(startPos.x/setup::TILESIZE, startPos.y / setup::TILESIZE, goalPos.x / setup::TILESIZE, goalPos.y / setup::TILESIZE);
					}

					else if (event.key.code == sf::Keyboard::R)
					{
						for (int i = 0; i < setup::GRIDWIDTH; i++)
						{
							for (int j = 0; j < setup::GRIDHEIGHT; j++)
							{
								tilemap[i][j]->Reset();
							}
						}
						RandStart();
						RandGoal();
					}
					//else if (event.type == sf::Mouse::isButtonPressed(sf::Mouse::Left)) //Sets the tile under the cursor as an obstacle and vice versa
					//{
					//	for ()
					//}
					break;
				default: 
					break;
		}

			//if (event.type == sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) //Starts movement for the character
			//{

			//}


		}

		window.clear();

		for (int i = 0; i < setup::GRIDWIDTH; i++)
		{
			for (int j = 0; j < setup::GRIDHEIGHT; j++)
			{
				tilemap[i][j]->Draw(window);
			}
		}
		window.draw(*characterSprite);
		window.display();
	}
    return 0;
}

