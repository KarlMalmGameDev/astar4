#pragma once
#include "Tile.h"
#include <iostream>
#include "Config.h"
#include "stdafx.h"
#include <cmath>

class AstarCalculator
{
public:
	AstarCalculator(Tile* Tilemap[setup::GRIDWIDTH][setup::GRIDHEIGHT]);
	std::vector<Tile*> CalculatePath(int startx, int endx, int starty, int endy);
	~AstarCalculator();

	Tile* gridreference[setup::GRIDWIDTH][setup::GRIDHEIGHT];
private:
	int fCheck = 500;
	std::vector<Tile*> openList;
	std::vector<Tile*> closedList;
	std::vector<Tile*> path;

	void FindNodes(int indexX, int indexY);
	std::pair<int, int> FindLowestF();
	void MoveToOpen(int indexX, int indexY, Tile* parentTile);
	void MoveToClosed(int indexX, int indexY);
	Tile* AStarRecurse(int startx, int endx, int starty, int endy);
	void CalculateFGH(int currentX, int currentY, int endX, int endY);
};

